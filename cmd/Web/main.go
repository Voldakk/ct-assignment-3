package main

import (
	"bitbucket.org/Voldakk/ct-assignment-3/app"
	"fmt"
	"net/http"
	"os"
)

func main() {

	app.ClockInit()
	app.DB.Init()

	app.GetPreviousRates()

	http.HandleFunc("/botlatest", app.HandlerBotLatest)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}
	fmt.Println(port)

	http.ListenAndServe(":"+port, nil)
}
