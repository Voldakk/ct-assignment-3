package main

import (
	"bitbucket.org/Voldakk/ct-assignment-3/app"
	"fmt"
	"net/http"
)

// Fixer.io updates around 	1600 CET or 1500 UTC
// Summer time:				1600 CEST = 1400 UTC

// Triggered at 1600 UTC. This wil result in a 1-2 hour margin, which is probably fine
func main() {
	fmt.Println("Triggered by the scheduler")
	app.Daily(http.DefaultClient.Post, http.DefaultClient.Get)
}
