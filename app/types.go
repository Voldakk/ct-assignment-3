package app

import (
	"io"
	"net/http"
)

type dbInfo struct {
	DatabaseURL        string
	DatabaseName       string
	WebHooksCollection string
	RatesCollection    string
}

type webhook struct {
	ID              string  `json:"ID"`
	WebhookURL      string  `json:"webhookURL"`
	BaseCurrency    string  `json:"baseCurrency"`
	TargetCurrency  string  `json:"targetCurrency"`
	MinTriggerValue float64 `json:"minTriggerValue"`
	MaxTriggerValue float64 `json:"maxTriggerValue"`
}

type webhookTrigger struct {
	Content         string  `json:"content"`
	BaseCurrency    string  `json:"baseCurrency"`
	TargetCurrency  string  `json:"targetCurrency"`
	CurrentRate     float64 `json:"currentRate"`
	MinTriggerValue float64 `json:"minTriggerValue"`
	MaxTriggerValue float64 `json:"maxTriggerValue"`
}

type baseTargetRequest struct {
	BaseCurrency   string `json:"baseCurrency"`
	TargetCurrency string `json:"targetCurrency"`
}

type currencyRates struct {
	Base  string             `json:"base"`
	Date  string             `json:"date"`
	Rates map[string]float64 `json:"rates"`
}

// Request from DialogFlow chatbot
type ChatbotRequest struct {
	Result struct {
		Parameters struct {
			Currency1 string `json:"Currency1"`
			Currency2 string `json:"Currency2"`
		} `json:"parameters"`
	} `json:"result"`
}

type ChatbotResponse struct {
	Speech   	string `json:"speech"`
	DisplayText string `json:"displayText"`
}

type funcGet func(string) (*http.Response, error)
type funcPost func(string, string, io.Reader) (*http.Response, error)
