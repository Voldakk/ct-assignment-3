package app

import (
	"fmt"
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"os"
	"time"
)

var dateFormat = "2006-01-02"

// DB contains connection info for the db
var DB = dbInfo{
	os.Getenv("DBURL"),
	"ct-assignment3",
	"webhooks",
	"rates"}

var testDB = dbInfo{
	os.Getenv("DBURL"),
	"ct-assignment3",
	"webhooks_test",
	"rates_test"}

// Init dials the db to check the connection
func (db *dbInfo) Init() {

	// Connect to the db
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	if err != nil {
		panic(err)
	}
}

func (db *dbInfo) count(cn string) (int, error) {

	// Connect to the db
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	if err != nil {
		return 0, err
	}

	// Get the collection
	c := session.DB(db.DatabaseName).C(cn)

	count, err := c.Count()

	if err != nil {
		return 0, err
	}

	return count, nil
}

func (db *dbInfo) registerWebhook(wh webhook) (string, error) {

	// Connect to the db
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	if err != nil {
		return "", err
	}

	// Get the collection
	c := session.DB(db.DatabaseName).C(db.WebHooksCollection)

	wh.ID = bson.NewObjectId().Hex()

	err = c.Insert(wh)
	if err != nil {
		return "", err
	}

	return wh.ID, nil
}

func (db *dbInfo) getWebhook(id string) (webhook, error) {

	wh := webhook{}

	// Connect to the db
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	if err != nil {
		return wh, err
	}

	// Get the collection
	c := session.DB(db.DatabaseName).C(db.WebHooksCollection)

	err = c.Find(bson.M{"id": id}).One(&wh)
	if err != nil {
		return wh, err
	}

	return wh, nil
}

// getAllWebhooks gets all the webhooks from the db
func (db *dbInfo) getAllWebhooks() ([]webhook, error) {

	// Connect to the db
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	if err != nil {
		return nil, err
	}

	// Get the collection
	c := session.DB(db.DatabaseName).C(db.WebHooksCollection)

	// Find all the webhooks in the db
	query := c.Find(nil).Sort("-date")

	// Return nil if there isn't any
	count, _ := query.Count()
	if count == 0 {
		return nil, mgo.ErrNotFound
	}

	// Create a slice
	whs := make([]webhook, count)

	// Read the result into the slice
	query.All(&whs)

	return whs, nil
}

func (db *dbInfo) deleteWebhook(id string) error {

	// Connect to the db
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	if err != nil {
		return err
	}

	// Get the collection
	c := session.DB(db.DatabaseName).C(db.WebHooksCollection)

	// Remove the webhook from the db
	err = c.Remove(bson.M{"id": id})
	if err != nil {
		fmt.Println(err.Error())
	}

	return err
}

func (db *dbInfo) registerRates(l currencyRates) error {

	// Connect to the db
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	if err != nil {
		return err
	}

	// Get the collection
	c := session.DB(db.DatabaseName).C(db.RatesCollection)

	err = c.Insert(l)
	if err != nil {
		return err
	}

	return nil
}

func (db *dbInfo) hasRates(t time.Time) (bool, error) {

	// Connect to the db
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	if err != nil {
		return false, err
	}

	// Get the collection
	c := session.DB(db.DatabaseName).C(db.RatesCollection)

	// Try to find rates from the given date
	r := currencyRates{}
	err = c.Find(bson.M{"date": t.Format(dateFormat)}).One(&r)

	// If it's found
	if err == nil {
		return true, nil

		// If it's not found
	} else if err == mgo.ErrNotFound {
		return false, nil

		// If other error
	} else {
		fmt.Println(err.Error())
		return false, err
	}
}

func (db *dbInfo) getLatestRate(btr baseTargetRequest) (float64, error) {

	// Connect to the db
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	if err != nil {
		return 0, err
	}

	// Get the collection
	c := session.DB(db.DatabaseName).C(db.RatesCollection)

	// Find the latest rate
	rate := currencyRates{}
	err = c.Find(bson.M{"base": btr.BaseCurrency}).Sort("-date").One(&rate)

	if err != nil {
		fmt.Println(err.Error())
		return 0, err
	}

	val, ok := rate.Rates[btr.TargetCurrency]

	if !ok {
		return 0, mgo.ErrNotFound
	}

	return val, nil
}

func (db *dbInfo) getLatestRates() (currencyRates, error) {

	rates := currencyRates{}

	// Connect to the db
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	if err != nil {
		return rates, err
	}

	// Get the collection
	c := session.DB(db.DatabaseName).C(db.RatesCollection)

	// Find the latest rate

	err = c.Find(nil).Sort("-date").One(&rates)

	if err != nil {
		fmt.Println(err.Error())
		return rates, err
	}

	return rates, nil
}

func (db *dbInfo) getAverageRate(btr baseTargetRequest) (float64, error) {

	// Connect to the db
	session, err := mgo.Dial(db.DatabaseURL)
	defer session.Close()
	if err != nil {
		return 0, err
	}

	// Get the collection
	c := session.DB(db.DatabaseName).C(db.RatesCollection)

	//c.Find(l)

	query := c.Find(bson.M{"base": btr.BaseCurrency}).Sort("-date").Limit(3)
	count, _ := query.Count()

	if count == 0 {
		return 0, mgo.ErrNotFound
	}

	crs := make([]currencyRates, count)
	query.All(&crs)

	total := 0.0
	for _, cr := range crs {
		if val, ok := cr.Rates[btr.TargetCurrency]; ok {
			total += val
		} else {
			return 0, mgo.ErrNotFound
		}
	}

	return total / float64(count), err
}

func clearTestWebHooksCollection() {
	session, err := mgo.Dial(testDB.DatabaseURL)
	if err != nil {
		return
	}
	defer session.Close()

	session.DB(testDB.DatabaseName).C(testDB.WebHooksCollection).RemoveAll(nil)
}

func clearTestRatesCollection() {
	session, err := mgo.Dial(testDB.DatabaseURL)
	if err != nil {
		return
	}
	defer session.Close()

	session.DB(testDB.DatabaseName).C(testDB.RatesCollection).RemoveAll(nil)
}
