package app

import (
	"strconv"
	"testing"
	"time"
)

func TestMongoDB_Init(t *testing.T) {
	testDB.Init()
}

func TestMongoDB_RegisterWebhook(t *testing.T) {

	clearTestWebHooksCollection()

	c, err := testDB.count(testDB.WebHooksCollection)
	if err != nil {
		t.Error("Error in count: " + err.Error())
	}
	if c != 0 {
		t.Error("Wrong count")
	}

	h := webhook{"", "127.0.01:8080", "EUR", "NOK", 1, 2}
	_, err = testDB.registerWebhook(h)
	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
	}

	c, err = testDB.count(testDB.WebHooksCollection)
	if err != nil {
		t.Error("Error in count: " + err.Error())
	}
	if c != 1 {
		t.Error("Wrong count")
	}
}

func TestMongoDB_GetWebhook(t *testing.T) {
	clearTestWebHooksCollection()

	h1 := webhook{"", "127.0.01:8080", "EUR", "NOK", 1, 2}
	id, err := testDB.registerWebhook(h1)
	h1.ID = id

	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
	}

	h2, err := testDB.getWebhook(id)
	if err != nil {
		t.Error("Error getting web hook from db: " + err.Error())
	}

	if h1.ID != h2.ID || h1.WebhookURL != h2.WebhookURL {
		t.Error("The web hooks are not the same: " + h1.ID + " | " + h2.ID)
	}
}

func TestMongoDB_GetAllWebhooks(t *testing.T) {
	clearTestWebHooksCollection()

	h1 := webhook{"", "127.0.01:8080", "EUR", "NOK", 1, 2}
	id, err := testDB.registerWebhook(h1)
	h1.ID = id

	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
	}

	h2, err := testDB.getAllWebhooks()
	if err != nil {
		t.Error("Error getting web hook from db: " + err.Error())
	}
	if len(h2) != 1 {
		t.Error("Wrong number of webhooks")
		return
	}

	if h1.ID != h2[0].ID || h1.WebhookURL != h2[0].WebhookURL {
		t.Error("The web hooks are not the same: " + h1.ID + " | " + h2[0].ID)
	}
}

func TestMongoDB_DeleteWebhook(t *testing.T) {
	clearTestWebHooksCollection()

	h1 := webhook{"", "127.0.01:8080", "EUR", "NOK", 1, 2}
	id, err := testDB.registerWebhook(h1)
	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
	}

	c, err := testDB.count(testDB.WebHooksCollection)
	if err != nil {
		t.Error("Error in count: " + err.Error())
	}

	if c != 1 {
		t.Error("Wrong count1: " + strconv.Itoa(c))
	}

	testDB.deleteWebhook(id)

	c, err = testDB.count(testDB.WebHooksCollection)
	if err != nil {
		t.Error("Error in count: " + err.Error())
	}

	if c != 0 {
		t.Error("Wrong count2: " + strconv.Itoa(c))
	}
}

func TestMongoDB_RegisterRates(t *testing.T) {
	clearTestRatesCollection()
	c, err := testDB.count(testDB.RatesCollection)
	if err != nil {
		t.Error("Error in count: " + err.Error())
	}
	if c != 0 {
		t.Error("Wrong count1: " + strconv.Itoa(c))
	}

	ra := make(map[string]float64)
	ra["NOK"] = 9
	r := currencyRates{"EUR", "2017-01-01", ra}

	err = testDB.registerRates(r)
	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
	}

	c, err = testDB.count(testDB.RatesCollection)
	if err != nil {
		t.Error("Error in count: " + err.Error())
	}
	if c != 1 {
		t.Error("Wrong count2: " + strconv.Itoa(c))
	}
}

func TestMongoDB_HasRates(t *testing.T) {
	clearTestRatesCollection()

	h, err := testDB.hasRates(time.Now())
	if err != nil {
		t.Error("Error is hasRates: " + err.Error())
	}
	if h == true {
		t.Error("hasRates is true, should be false")
	}

	ra := make(map[string]float64)
	ra["NOK"] = 9
	r := currencyRates{"EUR", time.Now().Format(dateFormat), ra}

	err = testDB.registerRates(r)
	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
	}

	h, err = testDB.hasRates(time.Now())
	if err != nil {
		t.Error("Error is hasRates: " + err.Error())
	}
	if h == false {
		t.Error("hasRates is false, should be true")
	}
}

func TestDBInfo_GetLatestRate(t *testing.T) {
	clearTestRatesCollection()

	r := make(map[string]float64)

	cr := currencyRates{"EUR", time.Now().Format(dateFormat), r}
	cr.Rates["NOK"] = 1000
	err := testDB.registerRates(cr)
	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
	}

	value, err := testDB.getLatestRate(baseTargetRequest{"EUR", "NOK"})

	if err != nil {
		t.Error("Error getting latest rate from db: " + err.Error())
	}
	if value != 1000 {
		t.Error("Wrong latest rate: " + strconv.FormatFloat(value, 'f', -1, 64))
	}
}

func TestDBInfo_GetLatestRates(t *testing.T) {
	clearTestRatesCollection()

	r := make(map[string]float64)

	cr := currencyRates{"EUR", time.Now().Format(dateFormat), r}
	cr.Rates["NOK"] = 1000
	cr.Rates["USD"] = 2000
	err := testDB.registerRates(cr)
	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
	}

	latest, err := testDB.getLatestRates()

	if err != nil {
		t.Error("Error getting latest rates from db: " + err.Error())
	}
	if latest.Base != "EUR" || latest.Date != time.Now().Format(dateFormat) ||
		latest.Rates["NOK"] != 1000 || latest.Rates["USD"] != 2000 {
		t.Error("Wrong latest rates")
	}
}

func TestDBInfo_GetAverageRate(t *testing.T) {
	clearTestRatesCollection()

	r := make(map[string]float64)

	cr := currencyRates{"EUR", time.Now().Format(dateFormat), r}
	cr.Rates["NOK"] = 1000
	err := testDB.registerRates(cr)
	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
	}

	cr = currencyRates{"EUR", time.Now().Add(-time.Hour * 24).Format(dateFormat), r}
	cr.Rates["NOK"] = 2000
	err = testDB.registerRates(cr)
	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
	}

	cr = currencyRates{"EUR", time.Now().Add(-time.Hour * 48).Format(dateFormat), r}
	cr.Rates["NOK"] = 6000
	err = testDB.registerRates(cr)
	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
	}

	value, err := testDB.getAverageRate(baseTargetRequest{"EUR", "NOK"})
	if err != nil {
		t.Error("Error getting average rate from db: " + err.Error())
	}

	if value != 3000 {
		t.Error("Wrong average rate: " + strconv.FormatFloat(value, 'f', -1, 64))
	}
}
