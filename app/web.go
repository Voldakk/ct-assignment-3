package app

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

// HandlerBotLatest handles "/latest"
func HandlerBotLatest(w http.ResponseWriter, r *http.Request) {

	// Read json
	cbr := ChatbotRequest{}
	data, err := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(data, &cbr)

	if err != nil || cbr.Result.Parameters.Currency1 == "" || cbr.Result.Parameters.Currency2 == "" {
		http.Error(w, http.StatusText(http.StatusBadRequest), http.StatusBadRequest)
		return
	}

	l := baseTargetRequest{cbr.Result.Parameters.Currency1, cbr.Result.Parameters.Currency2 }

	// Get the latest rate from the DB
	rate, err := DB.getLatestRate(l)
	if err != nil {
		http.Error(w, http.StatusText(http.StatusNotFound), http.StatusNotFound)
		return
	}

	text := fmt.Sprintf("The exchange rate between %v and %v is %v", l.BaseCurrency, l.TargetCurrency, rate)

	response := ChatbotResponse{ text, text }
	data, err = json.Marshal(response)
	// Print the rate
	fmt.Fprint(w, string(data))
}