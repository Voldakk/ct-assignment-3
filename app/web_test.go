package app

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHandlerBotLatest(t *testing.T) {
	DB = testDB
	clearTestRatesCollection()


	// Add test rates to db
	ra := make(map[string]float64)
	ra["NOK"] = 1000
	r := currencyRates{"EUR", "2017-01-01", ra}

	err := testDB.registerRates(r)
	if err != nil {
		t.Fatal("Error inserting rates to db: " + err.Error())
	}

	//
	// Correct Request
	//

	// Request
	creq := ChatbotRequest{}
	creq.Result.Parameters.Currency1 = "EUR"
	creq.Result.Parameters.Currency2 = "NOK"

	data, err := json.Marshal(creq)
	if err != nil {
		t.Fatal(err)
	}

	req, err := http.NewRequest("GET", "127.0.0.1/botlatest", ioutil.NopCloser(bytes.NewBufferString(string(data))))
	if err != nil {
		t.Fatal(err)
	}

	// ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(HandlerBotLatest)

	// Handle
	handler.ServeHTTP(rr, req)

	// Tests
	if status := rr.Code; status != http.StatusOK {
		t.Fatalf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	cresp := ChatbotResponse{}
	data, err = ioutil.ReadAll(rr.Body)
	err = json.Unmarshal(data, &cresp)
	if err != nil {
		t.Fatal("Error unmarshaling response: " + err.Error())
	}

	if cresp.DisplayText != "The exchange rate between EUR and NOK is 1000" {
		t.Error("Incorrect display text: " + cresp.DisplayText)
	}

	//
	// Incorrect Request
	//

	// Request
	data, err = json.Marshal(r)
	if err != nil {
		t.Fatal(err)
	}

	req, err = http.NewRequest("GET", "127.0.0.1/botlatest", ioutil.NopCloser(bytes.NewBufferString(string(data))))
	if err != nil {
		t.Fatal(err)
	}

	// ResponseRecorder (which satisfies http.ResponseWriter) to record the response.
	rr = httptest.NewRecorder()
	handler = http.HandlerFunc(HandlerBotLatest)

	// Handle
	handler.ServeHTTP(rr, req)

	// Tests
	if status := rr.Code; status != http.StatusBadRequest {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusBadRequest)
	}
}
