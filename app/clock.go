package app

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"sync"
	"time"
)

var updateHour = 0

func ClockInit() {
	env := os.Getenv("UPDATEHOUR")
	if env == "" {
		updateHour = 16
	} else {
		updateHour, _ = strconv.Atoi(env)
	}
}


// GetPreviousRates gets the rates from the 3 previous days
func GetPreviousRates() {
	fmt.Println("UH:" + strconv.Itoa(updateHour))
	utc := time.Now().UTC()
	if utc.Hour() > updateHour {

		// Get the rates from the 3 previous days (including today)
		getLatestRates(http.DefaultClient.Get)
		//getRates(time.Now().Add(-time.Hour*24), http.DefaultClient.Get)
		//getRates(time.Now().Add(-time.Hour*24*2), http.DefaultClient.Get)

	} else {

		// Get the rates from the 3 previous days (excluding today)
		getRates(time.Now().Add(-time.Hour*24), http.DefaultClient.Get)
		//getRates(time.Now().Add(-time.Hour*24*2), http.DefaultClient.Get)
		//getRates(time.Now().Add(-time.Hour*24*3), http.DefaultClient.Get)
	}
}

// getLatestRates gets the latest rates
func getLatestRates(getTarget func(string) (*http.Response, error)) {
	fmt.Println("Geting latest rates from fixer.io")
	getRates(time.Now().UTC(), getTarget)
}

// getRates gets the rates from a specific date
func getRates(t time.Time, getTarget funcGet) {
	fmt.Println("Geting rates for: " + t.Format(dateFormat))

	// Check if the rates for the given data is already in the DB
	h, _ := DB.hasRates(t)
	if h {
		fmt.Println("Rates already in DB")
		return
	}

	// Get rates for EUR
	err := getRate(t, getTarget, "EUR")
	if err != nil {
		fmt.Println("Unable to get rate EUR")
		return
	}
	crs, err := DB.getLatestRates()
	if err != nil {
		fmt.Println("Error getting rates: " + err.Error())
		return
	}

	// Get the rates for each other currency in the EUR rates
	for currency := range crs.Rates {
		getRate(t, getTarget, currency)
	}
}

func getRate(t time.Time, getTarget funcGet, base string) error {
	fmt.Printf("Getting rates, date: %v, base: %v \n", t.Format(dateFormat), base)

	// Get the data from fixer.io
	r, err := getTarget(fmt.Sprintf("https://api.fixer.io/%v?base=%v", t.Format(dateFormat), base))

	// Read json
	cr := currencyRates{}
	data, err := ioutil.ReadAll(r.Body)
	err = json.Unmarshal(data, &cr)

	if err != nil {
		fmt.Println("Error unmarshaling data from fixer: " + err.Error())
		fmt.Print(string(data))
		return err
	}

	// If fixer.io is missing data for a day
	// (e.g. 2017-10-29 and 2017-10-28 returns 2017-10-27)
	if cr.Date != t.Format(dateFormat) {
		fmt.Printf("Missing rates for %v. using values from %v instead\n", t.Format(dateFormat), cr.Date)

		// Pretend the data from the previous date is correct
		cr.Date = t.Format(dateFormat)
	}

	// Add the rates to the DB
	err = DB.registerRates(cr)

	if err != nil {
		fmt.Println(err.Error())
		return err
	}

	return nil
}

// Daily is triggered daily by Heroku Scheduler. It gets the latest rates and trigger webhooks
func Daily(postTarget funcPost, getTarget funcGet) {
	fmt.Printf("It's %v, doing the daily loop\n", time.Now().UTC().Format("15:04:05"))
	getLatestRates(getTarget)

	whs, err := DB.getAllWebhooks()
	if err != nil {
		fmt.Println("Error getting webhooks: " + err.Error())
		return
	} else if whs == nil {
		return
	}

	rates, err := DB.getLatestRates()
	if err != nil {
		fmt.Println("Error getting rates: " + err.Error())
		return
	}

	var wg sync.WaitGroup
	wg.Add(len(whs))

	for _, wh := range whs {

		go func(wh webhook) {
			defer wg.Done()

			current := rates.Rates[wh.TargetCurrency]
			if current > wh.MinTriggerValue && current < wh.MaxTriggerValue {
				wht := webhookTrigger{
					wh.ID, // Posts the id to discord for easier testing
					wh.BaseCurrency,
					wh.TargetCurrency,
					current,
					wh.MinTriggerValue,
					wh.MaxTriggerValue}

				data, err := json.Marshal(wht)
				if err != nil {
					fmt.Println("Error marshaling webhookTrigger: " + err.Error())
					return
				}

				r, err := postTarget(wh.WebhookURL, "application/json", bytes.NewReader(data))
				if err != nil {
					fmt.Printf("Error posting to webhook: %v\n%v", wh.WebhookURL, err.Error())
				} else {
					fmt.Printf("Recieved status %v from %v\n", r.StatusCode, wh.WebhookURL)
				}
			}
		}(wh)
	}

	// Wait until all the webhooks are processed
	wg.Wait()
}
