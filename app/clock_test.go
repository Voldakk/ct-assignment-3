package app

import (
	"bytes"
	"encoding/json"
	"io"
	"io/ioutil"
	"net/http"
	"testing"
	"time"
	"fmt"
)

func GetFromHere(url string) (*http.Response, error) {
	fmt.Println("GetFromHere: " + url)
	r := http.Response{}
	r.StatusCode = http.StatusOK
	r.Status = http.StatusText(r.StatusCode)
	r.Header = http.Header{}
	r.Header.Add("content-type", "application/json")

	rates := make(map[string]float64)
	rates["NOK"] = 1000
	rates["USD"] = 2000
	cr := currencyRates{"EUR", "2017-01-01", rates}

	data, _ := json.Marshal(cr)
	r.Body = ioutil.NopCloser(bytes.NewBufferString(string(data)))

	return &r, nil
}

var postRequests = make([]string, 0)

func PostToHere(url string, contentType string, body io.Reader) (*http.Response, error) {
	postRequests = append(postRequests, url)

	r := http.Response{}
	r.StatusCode = http.StatusOK
	r.Status = http.StatusText(r.StatusCode)
	r.Header = http.Header{}
	return &r, nil
}

func TestGetRates(t *testing.T) {
	DB = testDB

	clearTestRatesCollection()

	now := time.Date(2017, 01, 01, 0, 0, 0, 0, time.UTC)

	getRates(now, GetFromHere)

	cr, err := testDB.getLatestRates()

	if err != nil {
		t.Error("Error getting rates from db: " + err.Error())
		return
	}

	if cr.Base != "EUR" || cr.Date != "2017-01-01" || cr.Rates["NOK"] != 1000 || cr.Rates["USD"] != 2000 {
		t.Error("Wrong rates")
	}
}

func TestGetLatestRates(t *testing.T) {
	DB = testDB

	clearTestRatesCollection()

	getLatestRates(GetFromHere)

	cr, err := testDB.getLatestRates()

	if err != nil {
		t.Error("Error getting latest rates from db: " + err.Error())
		return
	}

	if cr.Base != "EUR" || cr.Date != time.Now().UTC().Format(dateFormat) || cr.Rates["NOK"] != 1000 || cr.Rates["USD"] != 2000 {
		fmt.Println("Date:" + cr.Date)
		t.Error("Wrong rates")
	}
}

func TestLoop(t *testing.T) {
	DB = testDB
	clearTestRatesCollection()
	clearTestWebHooksCollection()

	// Webhooks
	h := webhook{"", "Test-1", "EUR", "NOK", 999, 1001}
	_, err := testDB.registerWebhook(h)
	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
		return
	}
	h = webhook{"", "Test-2", "EUR", "NOK", 3000, 3001}
	_, err = testDB.registerWebhook(h)
	if err != nil {
		t.Error("Error inserting to db: " + err.Error())
		return
	}

	// Test
	postRequests = make([]string, 0)

	Daily(PostToHere, GetFromHere)

	if len(postRequests) != 1 {
		t.Error("Wrong number of post requests")
		return
	}
	if postRequests[0] != "Test-1" {
		t.Error("Wrong post request")
	}
}
